

;; My utilities to work with delimited files.
;; Do things like:
;;    a. Get list of all elements in a column.
;;    b. Get list of all elements in a row.
;;    c. Specifically get numbers.
;;
;; The idea is eventually we must be able to do reports on these things
;;    a. Detect if lines in a buffer contain fewer elements
;;    b. Perform artimetic on lists grabbed from rows and columns
;;    c. Do reports like average, mean, std deviation etc.
;;    d. Eventually do things like group by, order by, cube roll up
;;    e. Detect uniques. Distinct counts.
;;    f. Set operations like intersection, union, membership.
;;    g. Joins etc.
;;


;; Simple list of values from first column

;; Commenting these functions as we have better parameterized functions now
;; (defun list-numbers-first-column ()
;;   "Go to the start of the buffer, 
;;    get all numbers at first point at the beginning of the line, 
;;    till the end of the buffer"
;;   (save-excursion
;;     (if (not (bobp))
;;         (beginning-of-buffer))
;;     (get-numbers-first-column)))

;; (defun get-numbers-first-column ()
;;   "Returns all the numbers from, and below a point"
;;   (interactive) 
;;   (let ((acc '()))
;;     (while (not (eobp))
;;       (beginning-of-line)
;;       (let ((current-number (number-at-point)))
;;         (if current-number
;;             (setq acc (append acc (list current-number))))) 
;;       (if (eobp)
;;           (message "End of buffer reached --- Won't go the next line")
;;         (next-line)))
;;     acc))

;;Get the list of numbers from any column
(defun n1th (n l)
  "Use the 1 as the starting index to count from"
  (if (or (= 0 n)
          (> 0 n))
      (nth 0 l)
    (nth (1- n) l)))

;;(nth -1 '(1 2 3))
;;(n1th 0 '(1 2 3))
;;(n1th 1 '(1 2 3))
;;(n1th 2 '(1 2 3))
;;(n1th 10 '(1 2 3))

(defun listify-current-buffer (delimeter)
  "Listify current buffer, based on a delimeter"
  (save-excursion
    (beginning-of-buffer)
    (listify-lines delimeter)))

;;(listify-current-buffer " ")

(defun listify-buffer (buffer delimeter)
  "Go to a buffer, listify its lines based on delimeter"
  (save-excursion
    (switch-to-buffer buffer)
    (beginning-of-buffer)
    (listify-lines delimeter)))

(defun get-line ()
  (interactive)
  (buffer-substring-no-properties
   (line-beginning-position)
   (line-end-position)))

(defun listify-lines (delimiter)
  "Convert the buffer into a list of lists,
   lines being separated by a delimiter"
  (interactive)
  (let ((acc '()))
    (while (not (progn (end-of-line)
                       (eobp)))
      (beginning-of-line)
      (setq acc (push (split-string (get-line) delimiter) acc))
      (next-line)
      (if (progn (end-of-line)
                 (eobp))      
          (progn
            (beginning-of-line)
            (setq acc (push (split-string (get-line) delimiter) acc)))))
    acc))

(defun get-nth-column-current-buffer (delimeter n) 
  "Get the nth column from the current buffer, after splitting it using the delimeter"
  (interactive)
  (save-excursion
    (let ((lstfy (listify-current-buffer delimeter))
          (acc '()))
      (dolist (lst lstfy)
        (setq acc (push (n1th n lst) acc)))
      acc)))

(defun get-nth-column-current-buffer-no-nil (delimeter n)
  "Filter out nulls"
  (delete nil (mapcar
               (lambda (x)
                 (if x
                     x)))))

;;Commenting these functions as we have better parameterized functions now
;;Row related cuts
;; (defun list-numbers-row ()
;;   "Return all the numbers in a row, saves cursor point"
;;   (interactive)
;;   (save-excursion
;;     (get-numbers-list-row)))

;; (defun get-numbers-list-row ()
;;   "Return all the numbers from the start of a line(separated by spaces)"
;;   (interactive)
;;   (let ((acc '()))
;;     (end-of-line)
;;     (backward-word)
;;     (while (not (bolp)) 
;;       (let ((current-number (number-at-point)))
;;         (if current-number
;;             (setq acc (cons current-number acc))))
;;       (backward-word))
;;     (forward-word)
;;     (setq acc (cons (number-at-point) acc))
;;     acc))

(defun get-elements-list-row (d)
  "Get all the numbers in the current row, separated by delimiter"
  (interactive)
  (split-string (get-line) d))

(defun get-numbers (l)
  "Get list of numbers from a list of strings"
  (interactive)
  (delete nil (mapcar
               (lambda (x)
                 (if (or (string/string-integer-p x)
                         (string/string-float-p x))
                     (string-to-number x)
                   nil))
               l)))

(defun get-nth-row-current-buffer (delimeter n)
  "Return the numbers at a specific row in a list, numbers are delimited"
  (interactive)
  (let ((acc '()))
    (goto-line n)
    (get-numbers (get-elements-list-row delimeter))))
