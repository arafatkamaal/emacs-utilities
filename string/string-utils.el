

;;Recipes from the community and some by myself.
;;https://www.emacswiki.org/emacs/ElispCookbook


(defun string/starts-with (s begins)
  "Return non-nil if string S starts with BEGINS."
  (cond ((>= (length s) (length begins))
         (string-equal (substring s 0 (length begins)) begins))
        (t nil)))

(defun string/ends-with (string suffix)
  "Return t if STRING ends with SUFFIX."
  (and (string-match (rx-to-string `(: ,suffix eos) t)
                     string)
       t))

(defun string/reverse (str)
  "Reverse the str where str is a string"
  (apply #'string 
         (reverse 
          (string-to-list str))))

(defun string/chomp (str)
  "Chomp leading and tailing whitespace from STR."
  (replace-regexp-in-string (rx (or (: bos (* (any " \t\n")))
                                    (: (* (any " \t\n")) eos)))
                            ""
                            str))

(defun string/chomp-end (str)
  "Chomp tailing whitespace from STR."
  (replace-regexp-in-string (rx (* (any " \t\n")) eos)
                            ""
                            str))

(defun string/string-integer-p (string)
  (if (string-match "\\`[-+]?[0-9]+\\'" string)
      t
    nil))

(defun string/string-float-p (string)
  (if (string-match "\\`[-+]?[0-9]+\\.[0-9]*\\'" string)
      t
    nil))

(defun string/decimal-number (string)
  (let ((n (string-to-number string)))
    (save-match-data
      (if (and (not (zerop n))
               (string-match "\\`\\s-*0+\\.?0*\\s-*\\'" string))
          n
        nil))))

(defun string/group-number (num &optional size char)
  "Format NUM as string grouped to SIZE with CHAR."
  ;; Based on code for `math-group-float' in calc-ext.el
  (let* ((size (or size 3))
         (char (or char ","))
         (str (if (stringp num)
                  num
                (number-to-string num)))
         ;; omitting any trailing non-digit chars
         ;; NOTE: Calc supports BASE up to 36 (26 letters and 10 digits ;)
         (pt (or (string-match "[^0-9a-zA-Z]" str) (length str))))
    (while (> pt size)
      (setq str (concat (substring str 0 (- pt size))
                        char
                        (substring str (- pt size)))
            pt (- pt size)))
    str))

(defun string/map-regex (buffer regex fn)
  "Map the REGEX over the BUFFER executing FN.

   FN is called with the match-data of the regex.

   Returns the results of the FN as a list."
  (with-current-buffer buffer
    (save-excursion
      (goto-char (point-min))
      (let (res)
        (save-match-data
          (while (re-search-forward regex nil t)
            (let ((f (match-data)))
              (setq res
                    (append res
                            (list
                             (save-match-data
                               (funcall fn f))))))))
        res))))

(defun string/match-strings-all (&optional string)
  "Return the list of all expressions matched in last search.
  
  STRING is optionally what was given to `string-match'."
  (let ((n-matches (1- (/ (length (match-data)) 2))))
    (mapcar (lambda (i) (match-string i string))
            (number-sequence 0 n-matches))))

(defun string/regexp-list (regex string)
  "Return a list of all REGEXP matches in STRING."
  ;; source: http://emacs.stackexchange.com/questions/7148/get-all-regexp-matches-in-buffer-as-a-list
  (let ((pos 0)        ; string marker
        (matches ()))  ; return list
    (while (string-match regex string pos)
      (push (match-string 0 string) matches)
      (setq pos (match-end 0)))
    (setq matches (reverse matches))
    matches))

(defun string/keep-when (pred seq)
  (let ((del (make-symbol "del")))
    (remove del (mapcar (lambda (el)
                          (if (funcall pred el) el del)) seq))))
